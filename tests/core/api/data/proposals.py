from tests.core.api.utils.apitest import ApiTestElem, ApiTestExpected, ApiTestInput


test_data_proposal_list = [
    ApiTestElem(
        name="List proposals",
        input=ApiTestInput(
            login="abcd",
            route="/proposals",
        ),
        expected=ApiTestExpected(
            code=200,
        ),
    ),
    ApiTestElem(
        name="List proposals (admin)",
        input=ApiTestInput(
            login="efgh",
            route="/proposals",
        ),
        expected=ApiTestExpected(
            code=200,
        ),
    ),
    ApiTestElem(
        name="efgh / blc00001 / 404",
        input=ApiTestInput(
            login="efgh",
            route="/proposals/blc00001",
        ),
        expected=ApiTestExpected(
            code=404,
        ),
    ),
]
