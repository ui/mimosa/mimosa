from pydantic import BaseModel, Field


class BeamLineGroupBeamLine(BaseModel):
    beamLineName: str = Field(title="Beamline Name")
    archived: bool = Field(
        False,
        title="Archived",
        description="Whether this beamline is archived (no longer displayed on landing page)",
    )


class BeamLineGroup(BaseModel):
    groupName: str = Field(title="Group Name", descriptiopn="A group of beamlines")
    uiGroup: str = Field(title="UI Group", description="Display type to use in the UI")
    permission: str = Field(
        title="Permission",
        description="Permission required to view all proposals from these beamlines",
    )
    beamLines: list[BeamLineGroupBeamLine] = Field([], title="Beamlines")


class UIOptions(BaseModel):
    """Publicly available UI options"""

    motd: str = Field(
        "", title="Message of the Day", description="Displayed at the top of the UI"
    )
    beamLineGroups: list[BeamLineGroup] = Field([], title="Beamline Groups")


class Options(UIOptions):
    """All available application options"""

    query_debug: bool = Field(
        False, title="Query Debugging", description="Enable query debugging"
    )
