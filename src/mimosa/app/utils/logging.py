import logging

from starlette.types import ASGIApp, Scope, Receive, Send
from ..globals import g

logger = logging.getLogger(__name__)


class LoggingMiddleware:
    def __init__(
        self,
        app: ASGIApp,
    ) -> None:
        self.app = app

    async def __call__(self, scope: Scope, receive: Receive, send: Send) -> None:
        if scope["type"] not in ["http", "websocket"]:
            await self.app(scope, receive, send)
            return

        g.path = scope["path"]
        g.query_string = scope["query_string"]
        g.ip_address = scope["client"][0]

        await self.app(scope, receive, send)


class GrayPyFilter(logging.Filter):
    def filter(self, record):
        record.process_name = "mimosa"
        record.login = g.login
        record.personId = g.personId
        record.path = g.path
        record.query_string = g.query_string
        record.ip_address = g.ip_address

        return True
