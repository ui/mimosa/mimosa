import time
import logging
from functools import wraps

logger = logging.getLogger("mimosa")


def timed(fn):
    """
    Decorator to log the time that a class function takes to execute
    """

    @wraps(fn)
    def wrapper(self, *args, **kwargs):
        start = time.time()
        result = fn(self, *args, **kwargs)
        took = round(time.time() - start, 3)
        logger.debug(f"Class {self.__class__} - Function {fn.__name__}  took {took}")

        return result

    return wrapper
