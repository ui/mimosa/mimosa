import os

from functools import lru_cache
from pydantic import BaseSettings, BaseModel
import yaml

from .app.utils.logging import GrayPyFilter

PROJECT_ROOT = os.path.abspath(os.path.dirname(__file__))


class Settings(BaseSettings):
    static_root: str = os.path.join(PROJECT_ROOT, "static")

    api_root: str
    service_name: str

    sqlalchemy_database_uri: str
    query_debug: bool

    auth = {}
    auth_config: str

    jwt_coding_algorithm: str
    token_exp_time: int  # in minutes
    secret_key: str

    cors: bool = False

    simulation_config: str = None

    # Map file paths in the database to a different root directory
    path_map: str = None

    graylog_host: str
    graylog_port: int

    class Config:
        env_file = [".env", ".env.local"]


@lru_cache
def get_settings() -> Settings:
    return Settings()


settings = get_settings()

AUTH_CONFIG = os.path.realpath(
    os.path.join(PROJECT_ROOT, "..", "..", settings.auth_config)
)
try:
    with open(AUTH_CONFIG) as f:
        yaml_settings = dict()
        yaml_settings.update(yaml.safe_load(f))
        settings.auth = yaml_settings["AUTH"]
except IOError:
    raise Exception(f"Could not access auth config: {AUTH_CONFIG}")


class LogConfig(BaseModel):
    """Logging configuration to be set for the server"""

    LOGGER_NAME: str = "mimosa"
    LOG_FORMAT: str = "%(levelprefix)s | %(asctime)s | %(name)s | %(message)s"
    LOG_LEVEL: str = "INFO"

    version = 1
    disable_existing_loggers = False
    formatters = {
        "default": {
            "()": "uvicorn.logging.DefaultFormatter",
            "fmt": LOG_FORMAT,
            "datefmt": "%Y-%m-%d %H:%M:%S",
        },
        "gelf": {
            "()": "uvicorn.logging.DefaultFormatter",
            "fmt": "%(message)s",
            "datefmt": "%Y-%m-%d %H:%M:%S",
        },
    }
    filters = {
        "graypy": {
            "()": GrayPyFilter,
        }
    }
    handlers = {
        "default": {
            "formatter": "default",
            "class": "logging.StreamHandler",
            "stream": "ext://sys.stderr",
        },
        "gelf": {
            "formatter": "gelf",
            "class": "graypy.GELFUDPHandler",
            "host": settings.graylog_host,
            "port": settings.graylog_port,
            "level": "DEBUG",
            "filters": ["graypy"],
        },
    }
    loggers = {
        "root": {"handlers": ["default", "gelf"], "level": LOG_LEVEL},
        "mimosa": {"handlers": ["default", "gelf"], "level": LOG_LEVEL},
        "db": {"handlers": ["default", "gelf"], "level": "DEBUG"},
    }
