from typing import Optional

from sqlalchemy import distinct, func
from sqlalchemy.orm import joinedload
from mimosa import models

from ...app.extensions.database.definitions import with_authorization
from ...app.extensions.database.utils import Paged, page, with_metadata
from ...app.extensions.database.middleware import db


def get_dewars(
    skip: int,
    limit: int,
    dewarId: Optional[int] = None,
    code: Optional[str] = None,
    shippingId: Optional[int] = None,
    proposal: str = None,
    proposalId: Optional[int] = None,
    withAuthorization: bool = True,
) -> Paged[models.Dewar]:
    metadata = {"containers": func.count(distinct(models.Container.containerId))}

    query = (
        db.session.query(models.Dewar, *metadata.values())
        .join(models.Dewar.Shipping)
        .options(joinedload(models.Dewar.Shipping))
        .join(models.Proposal, models.Proposal.proposalId == models.Shipping.proposalId)
        .outerjoin(models.Container, models.Container.dewarId == models.Dewar.dewarId)
        .group_by(models.Dewar.dewarId)
        .order_by(models.Dewar.dewarId)
    )

    if dewarId:
        query = query.filter(models.Dewar.dewarId == dewarId)

    if code:
        query = query.filter(models.Dewar.code == code)

    if shippingId:
        query = query.filter(models.Shipping.shippingId == shippingId)

    if proposal:
        query = query.filter(models.Proposal.proposal == proposal)

    if proposalId:
        query = query.filter(models.Proposal.proposalId == proposalId)

    if withAuthorization:
        query = with_authorization(query)

    total = query.count()
    query = page(query, skip=skip, limit=limit)
    results = with_metadata(query.all(), list(metadata.keys()))

    return Paged(total=total, results=results, skip=skip, limit=limit)
