import io
import gzip
import json
import logging
import math
import os

import h5py
import h5grove
import matplotlib as mpl
import matplotlib.cm as cm
import numpy as np
from PIL import Image
from sqlalchemy import distinct, func
from sqlalchemy.orm import joinedload, aliased
from mimosa import models

from ...config import settings
from ...app.extensions.database.definitions import with_authorization
from ...app.extensions.database.utils import Paged, page, with_metadata, update_model
from ...app.extensions.database.middleware import db
from ..schemas import mapping as schema

from .events import get_events
from ..schemas.datacollections import DataCollection
from .data import _get_dataset_data

logger = logging.getLogger(__name__)
MAP_SCALINGS = {"linear": mpl.colors.Normalize, "logarithmic": mpl.colors.LogNorm}


def get_maps(
    skip: int,
    limit: int,
    xrfFluorescenceMappingId: int = None,
    xrfFluorescenceMappingROIId: int = None,
    scalar: str = None,
    dataCollectionId: int = None,
    dataCollectionGroupId: int = None,
    blSampleId: int = None,
    blSubSampleId: int = None,
    withAuthorization: bool = True,
) -> Paged[models.XRFFluorescenceMapping]:
    metadata = {
        "url": func.concat(
            f"{settings.api_root}/mapping/",
            models.XRFFluorescenceMapping.xrfFluorescenceMappingId,
        ),
        "blSubSampleId": models.DataCollection.blSubSampleId,
        "blSampleId": models.DataCollectionGroup.blSampleId,
        "dataCollectionId": models.DataCollection.dataCollectionId,
    }

    query = (
        db.session.query(models.XRFFluorescenceMapping, *metadata.values())
        .join(models.XRFFluorescenceMappingROI)
        .options(joinedload(models.XRFFluorescenceMapping.XRFFluorescenceMappingROI))
        .join(models.GridInfo)
        .options(joinedload(models.XRFFluorescenceMapping.GridInfo))
        .join(
            models.DataCollection,
            models.GridInfo.dataCollectionId == models.DataCollection.dataCollectionId,
        )
        .join(
            models.DataCollectionGroup,
            models.DataCollection.dataCollectionGroupId
            == models.DataCollectionGroup.dataCollectionGroupId,
        )
        .join(models.BLSession)
        .join(models.Proposal)
        .group_by(models.XRFFluorescenceMapping.xrfFluorescenceMappingId)
        .order_by(models.XRFFluorescenceMapping.xrfFluorescenceMappingId)
    )

    if xrfFluorescenceMappingId:
        query = query.filter(
            models.XRFFluorescenceMapping.xrfFluorescenceMappingId
            == xrfFluorescenceMappingId
        )

    if scalar:
        query = query.filter(models.XRFFluorescenceMappingROI.scalar == scalar)

    if dataCollectionId:
        query = query.filter(models.DataCollection.dataCollectionId == dataCollectionId)

    if dataCollectionGroupId:
        query = query.filter(
            models.DataCollectionGroup.dataCollectionGroupId == dataCollectionGroupId
        )

    if xrfFluorescenceMappingROIId:
        query = query.filter(
            models.XRFFluorescenceMappingROI.xrfFluorescenceMappingROIId
            == xrfFluorescenceMappingROIId
        )

    if blSampleId:
        query = query.filter(models.DataCollectionGroup.blSampleId == blSampleId)

    if blSubSampleId:
        query = query.filter(models.DataCollection.blSubSampleId == blSubSampleId)

    if withAuthorization:
        query = with_authorization(query, joinBLSession=False)

    total = query.count()
    query = page(query, skip=skip, limit=limit)
    results = with_metadata(query.all(), list(metadata.keys()))

    return Paged(total=total, results=results, skip=skip, limit=limit)


def update_map(
    xrfFluorescenceMappingId: int, map_: schema.MapBase
) -> models.XRFFluorescenceMapping:
    map_dict = map_.dict(exclude_unset=True)
    new_map = get_maps(
        xrfFluorescenceMappingId=xrfFluorescenceMappingId, skip=0, limit=1
    ).first

    update_model(new_map, map_dict)
    db.session.commit()

    return get_maps(
        xrfFluorescenceMappingId=xrfFluorescenceMappingId, skip=0, limit=1
    ).first


def delete_map(xrfFluorescenceMappingId: int) -> None:
    maps = get_maps(xrfFluorescenceMappingId=xrfFluorescenceMappingId, skip=0, limit=1)
    map = maps.first

    has_scalar = (
        map.xrfFluorescenceMappingROIId
        if map.XRFFluorescenceMappingROI.scalar
        else None
    )
    db.session.delete(map)
    db.session.commit()

    # If this is a scalar map also remove the ROI
    if has_scalar:
        roi = (
            db.session.query(models.XRFFluorescenceMappingROI)
            .filter(
                models.XRFFluorescenceMappingROI.xrfFluorescenceMappingROIId
                == has_scalar
            )
            .first()
        )
        if not roi:
            raise RuntimeError(f"Could not find scalar map ROI: `{has_scalar}`")

        db.session.delete(roi)
        db.session.commit()


def get_composite_maps(
    skip: int,
    limit: int,
    xfeFluorescenceCompositeId: int = None,
    xrfFluorescenceMappingId: int = None,
    dataCollectionId: int = None,
    dataCollectionGroupId: int = None,
    blSampleId: int = None,
    blSubSampleId: int = None,
    withAuthorization: bool = True,
) -> Paged[models.XFEFluorescenceComposite]:
    xrfmap2 = aliased(models.XRFFluorescenceMapping)
    xrfmap3 = aliased(models.XRFFluorescenceMapping)

    xrfmaproi2 = aliased(models.XRFFluorescenceMappingROI)
    xrfmaproi3 = aliased(models.XRFFluorescenceMappingROI)

    metadata = {
        "url": func.concat(
            f"{settings.api_root}/mapping/composites/",
            models.XFEFluorescenceComposite.xfeFluorescenceCompositeId,
        ),
        "blSubSampleId": models.DataCollection.blSubSampleId,
        "blSampleId": models.DataCollectionGroup.blSampleId,
        "rROI": func.ifnull(
            func.concat(
                models.XRFFluorescenceMappingROI.element,
                "-",
                models.XRFFluorescenceMappingROI.edge,
            ),
            models.XRFFluorescenceMappingROI.scalar,
        ),
        "gROI": func.ifnull(
            func.concat(xrfmaproi2.element, "-", xrfmaproi2.edge),
            xrfmaproi2.scalar,
        ),
        "bROI": func.ifnull(
            func.concat(xrfmaproi3.element, "-", xrfmaproi3.edge),
            xrfmaproi3.scalar,
        ),
    }

    query = (
        db.session.query(models.XFEFluorescenceComposite, *metadata.values())
        .join(
            models.XRFFluorescenceMapping,
            models.XRFFluorescenceMapping.xrfFluorescenceMappingId
            == models.XFEFluorescenceComposite.r,
        )
        .options(joinedload(models.XFEFluorescenceComposite.XRFFluorescenceMapping))
        .options(
            joinedload(
                models.XFEFluorescenceComposite.XRFFluorescenceMapping,
                models.XRFFluorescenceMapping.GridInfo,
            )
        )
        .join(
            models.XRFFluorescenceMappingROI,
            models.XRFFluorescenceMappingROI.xrfFluorescenceMappingROIId
            == models.XRFFluorescenceMapping.xrfFluorescenceMappingROIId,
        )
        .join(
            xrfmap2,
            xrfmap2.xrfFluorescenceMappingId == models.XFEFluorescenceComposite.g,
        )
        .join(
            xrfmaproi2,
            xrfmaproi2.xrfFluorescenceMappingROIId
            == xrfmap2.xrfFluorescenceMappingROIId,
        )
        .join(
            xrfmap3,
            xrfmap3.xrfFluorescenceMappingId == models.XFEFluorescenceComposite.b,
        )
        .join(
            xrfmaproi3,
            xrfmaproi3.xrfFluorescenceMappingROIId
            == xrfmap3.xrfFluorescenceMappingROIId,
        )
        .join(
            models.GridInfo,
            models.GridInfo.gridInfoId == models.XRFFluorescenceMapping.gridInfoId,
        )
        .join(
            models.DataCollection,
            models.GridInfo.dataCollectionId == models.DataCollection.dataCollectionId,
        )
        .join(
            models.DataCollectionGroup,
        )
        .join(models.BLSession)
        .join(models.Proposal)
        .group_by(models.XFEFluorescenceComposite.xfeFluorescenceCompositeId)
    )

    if xfeFluorescenceCompositeId:
        query = query.filter(
            models.XFEFluorescenceComposite.xfeFluorescenceCompositeId
            == xfeFluorescenceCompositeId
        )

    if xrfFluorescenceMappingId:
        query = query.filter(
            models.XRFFluorescenceMapping.xrfFluorescenceMappingId
            == xrfFluorescenceMappingId
        )

    if dataCollectionId:
        query = query.filter(models.DataCollection.dataCollectionId == dataCollectionId)

    if dataCollectionGroupId:
        query = query.filter(
            models.DataCollectionGroup.dataCollectionGroupId == dataCollectionGroupId
        )

    if blSampleId:
        query = query.filter(models.DataCollectionGroup.blSampleId == blSampleId)

    if blSubSampleId:
        query = query.filter(models.DataCollection.blSubSampleId == blSubSampleId)

    if withAuthorization:
        query = with_authorization(query, joinBLSession=False)

    total = query.count()
    query = page(query, skip=skip, limit=limit)
    results = with_metadata(query.all(), list(metadata.keys()))

    return Paged(total=total, results=results, skip=skip, limit=limit)


def create_composite_map(
    composite: schema.CompositeMapCreate,
) -> models.XFEFluorescenceComposite:
    composite_dict = composite.dict()

    for col in ["r", "g", "b"]:
        get_maps(
            xrfFluorescenceMappingId=getattr(composite, col), skip=0, limit=1
        ).first

    composite = models.XFEFluorescenceComposite(**composite_dict)
    db.session.add(composite)
    db.session.commit()

    new_composite = get_composite_maps(
        xfeFluorescenceCompositeId=composite.xfeFluorescenceCompositeId, skip=0, limit=1
    )
    return new_composite.first


def update_composite_map(
    xfeFluorescenceCompositeId: int, composite: schema.CompositeMap
) -> models.XFEFluorescenceComposite:
    composite_dict = composite.dict(exclude_unset=True)
    new_composite = get_composite_maps(
        xfeFluorescenceCompositeId=xfeFluorescenceCompositeId, skip=0, limit=1
    ).first

    update_model(new_composite, composite_dict)
    db.session.commit()

    return get_composite_maps(
        xfeFluorescenceCompositeId=xfeFluorescenceCompositeId, skip=0, limit=1
    ).first


def delete_composite_map(xfeFluorescenceCompositeId: int):
    composite_map = get_composite_maps(
        xfeFluorescenceCompositeId=xfeFluorescenceCompositeId, skip=0, limit=1
    ).first

    db.session.delete(composite_map)
    db.session.commit()


def get_map_rois(
    skip: int,
    limit: int,
    xrfFluorescenceMappingROIId: int = None,
    blSampleId: int = None,
    withAuthorization: bool = True,
) -> Paged[models.XRFFluorescenceMappingROI]:
    metadata = {
        "maps": func.count(
            distinct(models.XRFFluorescenceMapping.xrfFluorescenceMappingId)
        ),
    }
    query = (
        db.session.query(models.XRFFluorescenceMappingROI, *metadata.values())
        .join(models.BLSample)
        .join(models.Crystal)
        .join(models.Protein)
        .outerjoin(models.XRFFluorescenceMapping)
        .group_by(models.XRFFluorescenceMappingROI.xrfFluorescenceMappingROIId)
        .order_by(models.XRFFluorescenceMappingROI.xrfFluorescenceMappingROIId)
    )

    if xrfFluorescenceMappingROIId:
        query = query.filter(
            models.XRFFluorescenceMappingROI.xrfFluorescenceMappingROIId
            == xrfFluorescenceMappingROIId
        )

    if blSampleId:
        query = query.filter(models.BLSample.blSampleId == blSampleId)

    if withAuthorization:
        query = with_authorization(query, proposalColumn=models.Protein.proposalId)

    total = query.count()
    query = page(query, skip=skip, limit=limit)
    results = with_metadata(query.all(), list(metadata.keys()))

    return Paged(total=total, results=results, skip=skip, limit=limit)


def create_map_roi(
    roi: schema.MapROICreate,
) -> models.XRFFluorescenceMappingROI:
    roi_dict = roi.dict()

    roi = models.XRFFluorescenceMappingROI(**roi_dict)
    db.session.add(roi)
    db.session.commit()

    new_roi = get_map_rois(
        xrfFluorescenceMappingROIId=roi.xrfFluorescenceMappingROIId, skip=0, limit=1
    )
    return new_roi.first


def update_map_roi(
    xrfFluorescenceMappingROIId: int, roi: schema.MapROI
) -> models.XRFFluorescenceMappingROI:
    roi_dict = roi.dict(exclude_unset=True)
    new_roi = get_map_rois(
        xrfFluorescenceMappingROIId=xrfFluorescenceMappingROIId, skip=0, limit=1
    ).first

    update_model(new_roi, roi_dict)
    db.session.commit()

    return get_map_rois(
        xrfFluorescenceMappingROIId=xrfFluorescenceMappingROIId, skip=0, limit=1
    ).first


def delete_map_roi(xrfFluorescenceMappingROIId: int) -> None:
    rois = get_map_rois(
        xrfFluorescenceMappingROIId=xrfFluorescenceMappingROIId, skip=0, limit=1
    )
    roi = rois.first
    if roi._metadata["maps"] > 0:
        raise RuntimeError("Cannot remove ROI, it is in use by one or more maps")

    db.session.delete(roi)
    db.session.commit()


def create_scalar_map(
    scalarMap: schema.ScalarMapCreate,
) -> models.XRFFluorescenceMapping:
    dcs = get_events(dataCollectionId=scalarMap.dataCollectionId, skip=0, limit=1)
    dc = dcs.first
    existing = get_maps(
        dataCollectionId=scalarMap.dataCollectionId,
        scalar=scalarMap.scalar,
        skip=0,
        limit=1,
    )
    try:
        existing.first
        raise RuntimeError(f"Scalar map `{scalarMap.scalar}` already exists")
    except IndexError:
        pass

    dc_item: DataCollection = dc["Item"]
    file_path = f"{dc_item.imageDirectory}/{dc_item.fileTemplate}"
    if settings.path_map:
        file_path = settings.path_map + file_path
    h5path = dc_item.imageContainerSubPath

    if not os.path.exists(file_path):
        logger.warning(
            f"H5file `{file_path}` for dataCollectionId {dc['id']} does not exist on disk"
        )
        raise RuntimeError("File does not exist")

    with h5py.File(file_path, "r") as h5file:
        data = _get_dataset_data(h5file, dset_path=f"{h5path}/{scalarMap.scalar}")

        roi = models.XRFFluorescenceMappingROI(
            startEnergy=0, endEnergy=0, scalar=scalarMap.scalar
        )
        db.session.add(roi)
        db.session.commit()

        data = data.tolist()
        original_len = len(data)
        if original_len < dc_item.numberOfImages:
            missing = dc_item.numberOfImages - original_len
            data.extend([-1 for _ in range(missing)])

        map_ = models.XRFFluorescenceMapping(
            xrfFluorescenceMappingROIId=roi.xrfFluorescenceMappingROIId,
            gridInfoId=dc_item.GridInfo[0].gridInfoId,
            data=gzip_json(data),
            dataFormat="json+gzip",
            points=original_len,
        )

        db.session.add(map_)
        db.session.commit()

        return get_maps(
            xrfFluorescenceMappingId=map_.xrfFluorescenceMappingId, skip=0, limit=1
        ).first


def regenerate_maps(
    blSampleId: int,
    dataCollectionId: int = None,
    ev_per_bin: float = 5,
    ev_offset: float = 0,
) -> list[int]:
    """Re-generate ROI maps for a sample, and/or data collection"""
    rois = get_map_rois(blSampleId=blSampleId, skip=0, limit=9999)
    if dataCollectionId:
        dcs = get_events(dataCollectionId=dataCollectionId, skip=0, limit=1)
    else:
        dcs = get_events(blSampleId=blSampleId, skip=0, limit=9999)

    maps = get_maps(
        blSampleId=blSampleId, dataCollectionId=dataCollectionId, skip=0, limit=9999
    )
    existing = {}
    for map_ in maps.results:
        if not map_._metadata["dataCollectionId"] in existing:
            existing[map_._metadata["dataCollectionId"]] = {}

        if (
            map_.XRFFluorescenceMappingROI.element
            and map_.XRFFluorescenceMappingROI.edge
        ):
            existing[map_._metadata["dataCollectionId"]][
                f"{map_.XRFFluorescenceMappingROI.element}-{map_.XRFFluorescenceMappingROI.edge}"
            ] = map_.xrfFluorescenceMappingId

    created = []
    for dc in dcs.results:
        to_create = []
        for roi in rois.results:
            roi_name = f"{roi.element}-{roi.edge}"
            if dc["id"] in existing:
                if roi_name not in existing[dc["id"]]:
                    to_create.append(roi)

        if to_create:
            dc_item: DataCollection = dc["Item"]
            file_path = f"{dc_item.imageDirectory}/{dc_item.fileTemplate}"
            if settings.path_map:
                file_path = settings.path_map + file_path
            h5path = dc_item.imageContainerSubPath

            if not os.path.exists(file_path):
                logger.warning(
                    f"H5file `{file_path}` for dataCollectionId {dc['id']} does not exist on disk"
                )
                raise RuntimeError("File does not exist")

            with h5py.File(file_path, "r") as h5file:
                dset_content = h5grove.create_content(h5file, h5path)
                metadata = dset_content.metadata()

                spectra = []
                for child in metadata["children"]:
                    if len(child.get("shape")) == 2:
                        spectra.append(
                            {
                                "name": child["name"],
                                "data": _get_dataset_data(
                                    h5file, dset_path=f"{h5path}/{child['name']}"
                                ),
                                "shape": child["shape"],
                            }
                        )

                for roi in to_create:
                    spectrum = spectra[0]
                    spectrum_data = spectrum["data"]

                    roi_map = np.full((dc["Item"].numberOfImages), -1)
                    roi_start = max(
                        0, math.floor((roi.startEnergy - ev_offset) / ev_per_bin)
                    )
                    roi_end = min(
                        math.ceil((roi.endEnergy - ev_offset) / ev_per_bin),
                        len(spectrum_data),
                    )
                    mask = np.ones(len(spectrum_data[0]))
                    mask[roi_start:roi_end] = 1.0
                    points = np.sum(spectrum_data * mask, axis=1)
                    roi_map[0 : len(points)] = points

                    map_ = models.XRFFluorescenceMapping(
                        xrfFluorescenceMappingROIId=roi.xrfFluorescenceMappingROIId,
                        gridInfoId=dc_item.GridInfo[0].gridInfoId,
                        data=gzip_json(roi_map.tolist()),
                        dataFormat="json+gzip",
                        points=len(spectrum_data),
                    )

                    db.session.add(map_)
                    db.session.commit()

                    created.append(map_.xrfFluorescenceMappingId)

    return created


def shape_map(map_: schema.Map) -> np.ndarray:
    """Shapes a 1d map array into the correct 2d image

    Reorders the data if needbe for snaked collection
    Reshapes if the data was collected vertically

    Returns:
        data (ndarray): The XRF map data
    """
    if map_.dataFormat == "json+gzip":
        data = gunzip_json(map_.data)
        data = np.array(data)
    else:
        data = np.array(map_.data)

    # TODO: Catch raise
    if map_.GridInfo.orientation == "vertical":
        data = data.reshape(int(map_.GridInfo.steps_x), int(map_.GridInfo.steps_y))
        data = np.rot90(data)
        data = np.flipud(data)
    else:
        data = data.reshape(int(map_.GridInfo.steps_y), int(map_.GridInfo.steps_x))

    # For snaked collection every other row is reversed
    if map_.GridInfo.snaked:
        data[1::2, :] = data[1::2, ::-1]

    return data


def generate_map_image(map_: schema.Map, image_format: str = "PNG") -> io.BytesIO:
    """Generates a PIL Image from an XRF map

    -1 placeholder values are converted to a transparent pixel

    Returns:
        image (io.Bytes): Bytes of PIL Image
    """
    data = shape_map(map_)
    norm_alg = MAP_SCALINGS.get(map_.scale, MAP_SCALINGS["linear"])
    norm = norm_alg(vmin=map_.min, vmax=map_.max)

    colourmap = map_.colourMap or "viridis"
    if not hasattr(cm, colourmap):
        colourmap = "viridis"

    cmap = getattr(cm, colourmap or "viridis")

    m = cm.ScalarMappable(norm=norm, cmap=cmap)
    img_data = m.to_rgba(data, bytes=True)

    mask = data == -1
    img_data[mask, :] = [255, 255, 255, 0]

    image = Image.fromarray(img_data, "RGBA")
    img_io = io.BytesIO()
    image.save(img_io, image_format, quality=100)
    img_io.seek(0)

    return img_io


def generate_composite_map_image(
    xfeFluorescenceComposite: models.XFEFluorescenceComposite,
    maps: dict[str, models.XRFFluorescenceMapping],
    image_format: str = "PNG",
) -> io.BytesIO:
    """Generates a PIL Image from an XRF composite map

    Returns:
        image (Image): A PIL image
    """

    layers = []
    for col in ["r", "g", "b"]:
        map_ = maps[col]
        data = shape_map(map_)

        norm = mpl.colors.Normalize(vmin=map_.min, vmax=map_.max)
        normalised = (
            norm(data) * 255 * getattr(xfeFluorescenceComposite, f"{col}Opacity")
        )

        layers.append(normalised)

    layers.append(
        np.full(
            (int(map_.GridInfo.steps_y), int(map_.GridInfo.steps_x)),
            255,
            # round(xfeFluorescenceComposite.opacity * 255),
        )
    )

    img_data = np.dstack(layers).astype(np.uint8)
    image = Image.fromarray(img_data, "RGBA")
    img_io = io.BytesIO()
    image.save(img_io, image_format, quality=100)
    img_io.seek(0)

    return img_io


def generate_histogram(map_):
    """Generates a histogram of map data

    Args:
        map_(dict): An XRF map from the metadata handler

    Returns:
        data: (dict(list)): The histogram, bins, and widths
    """
    data = shape_map(map_)
    ndata = np.array(data)
    rdata = np.where(ndata == -1, 0, ndata)

    try:
        hist, bins = np.histogram(rdata, bins=50)
        center = (bins[:-1] + bins[1:]) / 2
        width = np.diff(bins)

    # TODO: This should not happen
    except (OverflowError, ValueError):
        hist = []
        center = []
        width = []

    return {
        "xrfFluorescenceMappingId": map_.xrfFluorescenceMappingId,
        "hist": hist.tolist(),
        "bins": center.tolist(),
        "width": width.tolist(),
    }


def gunzip_json(bytes_obj: str):
    """Un gzips a bytes object and load into json

    Returns:
        data(dict): The decoded json as a python object
    """
    if not bytes_obj:
        return []

    in_ = io.BytesIO()
    in_.write(bytes_obj)
    in_.seek(0)
    with gzip.GzipFile(fileobj=in_, mode="rb") as fo:
        gunzipped_bytes_obj = fo.read()

    return json.loads(gunzipped_bytes_obj.decode())


def gzip_json(obj):
    """Gzips a json dump of a python object

    Args:
        obj(dict): An object

    Returns:
        data(BytesIO): The binary compressed data
    """
    json_str = json.dumps(obj)

    out = io.BytesIO()
    with gzip.GzipFile(fileobj=out, mode="w") as fo:
        fo.write(json_str.encode())

    return out.getvalue()
