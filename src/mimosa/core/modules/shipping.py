from typing import Optional

from sqlalchemy import distinct, func
from sqlalchemy.orm import joinedload
from mimosa import models

from ...app.extensions.database.definitions import with_authorization
from ...app.extensions.database.utils import Paged, page, with_metadata
from ...app.extensions.database.middleware import db


def get_shippings(
    skip: int,
    limit: int,
    shippingId: Optional[int] = None,
    shippingName: Optional[str] = None,
    proposal: str = None,
    proposalId: Optional[int] = None,
    withAuthorization: bool = True,
) -> Paged[models.Shipping]:
    metadata = {"dewars": func.count(distinct(models.Dewar.dewarId))}

    query = (
        db.session.query(models.Shipping, *metadata.values())
        .options(joinedload(models.Shipping.LabContact))
        .options(joinedload(models.Shipping.LabContact1))
        .join(models.Proposal, models.Proposal.proposalId == models.Shipping.proposalId)
        .outerjoin(models.Dewar)
        .group_by(models.Shipping.shippingId)
    )

    if shippingId:
        query = query.filter(models.Shipping.shippingId == shippingId)

    if shippingName:
        query = query.filter(models.Shipping.shippingName == shippingName)

    if proposal:
        query = query.filter(models.Proposal.proposal == proposal)

    if proposalId:
        query = query.filter(models.Proposal.proposalId == proposalId)

    if withAuthorization:
        query = with_authorization(query)

    total = query.count()
    query = page(query, skip=skip, limit=limit)
    results = with_metadata(query.all(), list(metadata.keys()))

    return Paged(total=total, results=results, skip=skip, limit=limit)
