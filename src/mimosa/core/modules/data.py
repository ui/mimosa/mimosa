import logging
import os
from typing import Optional, Tuple

import h5grove
from h5grove.content import DatasetContent
import h5py
import numpy as np

from ...config import settings
from ...core.modules.events import get_events
from ...core.modules.processings import get_processing_attachments

logger = logging.getLogger(__name__)


def compute_histogram(data: np.ndarray) -> Tuple[np.ndarray, list]:
    # Mask the max value
    max_value = np.iinfo(data.dtype).max
    clean_data = data[data < max_value]
    max_data_value = np.max(clean_data)
    # Even though the data might be 32bit, the max value could be 65535
    if max_data_value < max_value and max_data_value == 65535:
        clean_data = data[data < max_data_value]
        max_value = max_data_value

    std = 3 * np.std(clean_data)
    mean = np.mean(clean_data)
    clean_data = clean_data[clean_data < mean + std]

    if clean_data.size == 0:
        return np.ndarray([]), [], 0

    hist, bins = np.histogram(
        clean_data.flatten(),
        bins=(
            np.arange(np.min(clean_data), np.max(clean_data), 1)
            if np.max(clean_data) <= 300
            else 300
        ),
    )
    return hist, bins.tolist(), max_value


def _get_dataset_data(
    h5file: h5py.File, dset_path: str, selection: Optional[str] = None
):
    dset_content = h5grove.create_content(h5file, dset_path)
    assert isinstance(dset_content, DatasetContent)  # nosec
    return dset_content.data(selection)


def get_file_ext(file_name: str):
    _, ext = os.path.splitext(file_name)
    return ext[1:]  # Remove leading dot


def get_h5_file(
    dataCollectionId: Optional[int] = None,
    autoProcProgramAttachmentId: Optional[int] = None,
    robotActionId: Optional[int] = None,
):
    """Find the relevant hdf5 file from the parameters"""

    # Direct datacollection hdf5
    if dataCollectionId is not None:
        datacollections = get_events(dataCollectionId=dataCollectionId, skip=0, limit=1)
        dc = datacollections.first["Item"]
        return os.path.join(dc.imageDirectory, dc.fileTemplate)

    #  Directly from autoprocprogramattachmentid
    elif autoProcProgramAttachmentId is not None:
        attachments = get_processing_attachments(
            autoProcProgramAttachmentId=autoProcProgramAttachmentId, skip=0, limit=1
        )
        attachment = attachments.first
        ext = get_file_ext(attachment.fileName).lower()
        if attachment.fileType == "Result" and ext in ["h5", "hdf5"]:
            file_path = os.path.join(attachment.filePath, attachment.fileName)
            return file_path

    # From a sample action
    elif robotActionId is not None:
        sampleactions = get_events(robotActionId, skip=0, limit=1)
        sampleaction = sampleactions.first
        return sampleaction.resultFilePath

    return None


def get_h5_path_mapped(**kwargs) -> str:
    file_path = get_h5_file(**kwargs)
    if file_path:
        if settings.path_map:
            return settings.path_map + file_path
        return file_path
