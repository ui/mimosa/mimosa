from typing import Optional
from pydantic import BaseModel, Field


class MapROICreate(BaseModel):
    element: str = Field(title="Element")
    edge: str = Field(title="Edge")
    startEnergy: float = Field(title="Start Energy", unit="eV")
    endEnergy: float = Field(title="End Energy", unit="eV")
    blSampleId: int


class ScalarMapCreate(BaseModel):
    dataCollectionId: int
    scalar: str = Field(title="Scalar Name")


class RegenerateMaps(BaseModel):
    blSampleId: int
    dataCollectionId: Optional[int]
    ev_per_bin: Optional[float] = Field(5, description="eV per bin for MCA", unit="eV")
    ev_offset: Optional[float] = Field(
        0, description="eV zero offset for MCA", unit="eV"
    )


class RegeneratedMapsResponse(BaseModel):
    xrfFluorescenceMappingIds: list[int]


class MapROIMetaData(BaseModel):
    maps: Optional[int]


class MapROI(BaseModel):
    xrfFluorescenceMappingROIId: int
    element: Optional[str]
    edge: Optional[str]
    scalar: Optional[str]
    startEnergy: float
    endEnergy: float

    metadata: MapROIMetaData = Field(alias="_metadata")

    class Config:
        orm_mode = True


class MapGridInfo(BaseModel):
    gridInfoId: int
    steps_x: int
    steps_y: int
    snaked: bool
    orientation: str

    class Config:
        orm_mode = True


class MapMetaData(BaseModel):
    url: str = Field(description="Url to map image")
    blSubSampleId: Optional[int]
    blSampleId: Optional[int]
    dataCollectionId: Optional[int]


class MapBase(BaseModel):
    colourMap: Optional[str]
    opacity: Optional[float]
    scale: Optional[str]

    min: Optional[float] = None
    max: Optional[float] = None


class Map(MapBase):
    xrfFluorescenceMappingId: int
    colourMap: Optional[str]
    opacity: Optional[float]
    points: Optional[int]
    dataFormat: str

    metadata: MapMetaData = Field(alias="_metadata")

    GridInfo: MapGridInfo
    XRFFluorescenceMappingROI: MapROI

    class Config:
        orm_mode = True


class MapHistogram(BaseModel):
    xrfFluorescenceMappingId: int
    hist: list[int]
    bins: list[float]
    width: list[float]


class MapPixelValue(BaseModel):
    xrfFluorescenceMappingId: int
    x: int
    y: int
    value: float


class MapPixelValues(BaseModel):
    dataCollectionGroupId: int
    xrfFluorescenceMappingROIId: int
    x: Optional[int] = None
    y: Optional[int] = None
    value: list[float]


class CompositeMapCreate(BaseModel):
    r: int
    g: int
    b: int

    rOpacity: Optional[float] = 1
    gOpacity: Optional[float] = 1
    bOpacity: Optional[float] = 1

    opacity: Optional[float] = 1


class CompositeMapMetaData(BaseModel):
    url: str = Field(description="Url to map image")
    blSubSampleId: int
    blSampleId: int

    rROI: Optional[str]
    gROI: Optional[str]
    bROI: Optional[str]


class CompositeMapMap(BaseModel):
    xrfFluorescenceMappingId: int
    points: Optional[int]

    GridInfo: MapGridInfo

    class Config:
        orm_mode = True


class CompositeMap(CompositeMapCreate):
    xfeFluorescenceCompositeId: int

    metadata: CompositeMapMetaData = Field(alias="_metadata")
    XRFFluorescenceMapping: CompositeMapMap

    class Config:
        orm_mode = True
