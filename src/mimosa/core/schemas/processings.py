import enum
from datetime import datetime
from typing import Optional

from pydantic import BaseModel, Field, validator


class StatusEnum(enum.Enum):
    RUNNING = None
    FAILED = 0
    SUCCESS = 1
    DIDNTRUN = 2


class ProcessingStatus(BaseModel):
    status: Optional[StatusEnum]

    @validator("status", pre=True)
    def check_status(cls, status):
        if status == "SUCCESS":
            return 1
        if status == "FAILED":
            return 0
        if status == "RUNNING":
            return None

        return status


class ProcessingProcessingStatus(ProcessingStatus):
    autoProcProgramId: int
    automatic: bool


class ProcessingStatuses(BaseModel):
    processing: Optional[dict[str, list[ProcessingProcessingStatus]]]


class ProcessingStatusesList(BaseModel):
    statuses: dict[int, ProcessingStatuses]


class ProcessingJobParameter(BaseModel):
    parameterKey: Optional[str]
    parameterValue: Optional[str]

    class Config:
        orm_mode = True


class ProcessingJob(BaseModel):
    processingJobId: int
    displayName: Optional[str]
    comments: Optional[str]
    recordTimestamp: datetime
    recipe: Optional[str]
    automatic: bool

    ProcessingJobParameters: Optional[list[ProcessingJobParameter]]

    class Config:
        orm_mode = True


class AutoProcProgramMessageSeverity(str, enum.Enum):
    ERROR = "ERROR"
    WARNING = "WARNING"
    INFO = "INFO"


class AutoProcProgramMessage(BaseModel):
    autoProcProgramMessageId: int
    autoProcProgramId: int
    description: str
    message: str
    severity: AutoProcProgramMessageSeverity
    recordTimeStamp: datetime

    class Config:
        orm_mode = True


class AutoProcProgramMetadata(BaseModel):
    attachments: Optional[int] = Field(description="Number of attachments")
    autoProcProgramMessages: Optional[list[AutoProcProgramMessage]]
    imageSweepCount: Optional[int]


class AutoProcProgram(BaseModel):
    autoProcProgramId: int
    processingCommandLine: Optional[str]
    processingPrograms: Optional[str]
    processingStatus: Optional[StatusEnum]
    processingMessage: Optional[str]
    processingStartTime: Optional[datetime]
    processingEndTime: Optional[datetime]
    processingEnvironment: Optional[str]
    recordTimeStamp: datetime

    ProcessingJob: Optional[ProcessingJob]

    metadata: AutoProcProgramMetadata = Field(alias="_metadata")

    @validator("processingStatus", pre=True)
    def check_status(cls, status):
        if status == "SUCCESS":
            return 1
        if status == "FAILED":
            return 0
        if status == "RUNNING":
            return None

        return status

    class Config:
        orm_mode = True


class AutoProcProgramMessageStatus(BaseModel):
    errors: int
    warnings: int
    info: int


class AutoProcProgramMessageStatuses(BaseModel):
    statuses: dict[int, AutoProcProgramMessageStatus]


class AutoProcProgramAttachmentMetaData(BaseModel):
    url: str = Field(description="Url to autoproc program attachment")


class AutoProcProgramAttachment(BaseModel):
    autoProcProgramAttachmentId: int
    autoProcProgramId: int
    fileName: str
    filePath: str
    fileType: str
    importanceRank: Optional[int]

    metadata: AutoProcProgramAttachmentMetaData = Field(alias="_metadata")

    class Config:
        orm_mode = True
