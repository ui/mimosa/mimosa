from pydantic import BaseModel


class DataHistogram(BaseModel):
    values: list[int]
    bins: list[int]
    shape: tuple
    max: float
    mask_value: float
