from typing import Optional

from pydantic import BaseModel, Field
from mimosa import models

from .protein import Protein

c = models.Crystal


class CrystalBase(BaseModel):
    cell_a: Optional[float] = Field(title="Cell A", nullable=True)
    cell_b: Optional[float] = Field(title="Cell B", nullable=True)
    cell_c: Optional[float] = Field(title="Cell C", nullable=True)
    cell_alpha: Optional[float] = Field(title="Cell Alpha", nullable=True)
    cell_beta: Optional[float] = Field(title="Cell Beta", nullable=True)
    cell_gamma: Optional[float] = Field(title="Cell Gamma", nullable=True)
    Protein: Protein


class Crystal(CrystalBase):
    crystalId: int
    proteinId: int = Field(title="Protein")

    Protein: Protein

    class Config:
        orm_mode = True
