from typing import Optional, Literal

from pydantic import BaseModel, Field


class RobotActionMetaData(BaseModel):
    snapshots: dict[Literal["before", "after"], bool] = Field(
        description="Snapshot statuses with keys `before` and `after`"
    )


class RobotAction(BaseModel):
    robotActionId: int
    actionType: str
    status: Optional[str]
    message: Optional[str]
    resultFilePath: Optional[str]

    metadata: RobotActionMetaData = Field(alias="_metadata")

    class Config:
        orm_mode = True
