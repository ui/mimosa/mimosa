from typing import Optional

from fastapi import Depends, HTTPException
from mimosa import models

from ...dependencies import pagination
from ...app.extensions.database.utils import Paged
from ... import filters
from ...app.base import AuthenticatedAPIRouter

from ..modules import proposals as crud
from ..schemas import proposals as schema
from ..schemas.utils import paginated


router = AuthenticatedAPIRouter(prefix="/proposals", tags=["Proposals"])


@router.get("", response_model=paginated(schema.Proposal))
def get_proposals(
    search: str = Depends(filters.search),
    page: dict[str, int] = Depends(pagination),
    proposalFilter: Optional[crud.PROPOSAL_FILTER_ENUM] = None,
) -> Paged[models.Proposal]:
    """Get a list of proposals"""
    return crud.get_proposals(search=search, proposalFilter=proposalFilter, **page)


@router.get(
    "/{proposal}",
    response_model=schema.Proposal,
    responses={404: {"description": "No such proposal"}},
)
def get_proposal(
    proposal: str,
) -> models.Proposal:
    """Get a proposal"""
    proposals = crud.get_proposals(
        proposal=proposal,
        skip=0,
        limit=1,
    )

    try:
        return proposals.first
    except IndexError:
        raise HTTPException(status_code=404, detail="Proposal not found")
