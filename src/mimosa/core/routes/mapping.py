import logging
from typing import Optional

from fastapi import Depends, HTTPException, Query, status
from fastapi.responses import StreamingResponse
from mimosa import models
import numpy

from ...dependencies import pagination
from ...app.extensions.database.utils import Paged
from ...app.base import AuthenticatedAPIRouter
from ... import filters

from ..modules import mapping as crud
from ..schemas import mapping as schema
from ..schemas.utils import paginated, make_optional


logger = logging.getLogger(__name__)
router = AuthenticatedAPIRouter(prefix="/mapping", tags=["Mapping"])


@router.get("/rois", response_model=paginated(schema.MapROI))
def get_map_rois(
    blSampleId: str = Depends(filters.blSampleId),
    xrfFluorescenceMappingROIId: Optional[int] = Query(
        None, title="xrfFluorescenceMapping ROI id"
    ),
    page: dict[str, int] = Depends(pagination),
) -> Paged[models.XRFFluorescenceMappingROI]:
    """Get a list of map rois"""
    return crud.get_map_rois(
        blSampleId=blSampleId,
        xrfFluorescenceMappingROIId=xrfFluorescenceMappingROIId,
        **page,
    )


@router.post(
    "/rois",
    response_model=schema.MapROI,
    status_code=status.HTTP_201_CREATED,
    responses={
        404: {"description": "No such sample"},
        400: {"description": "Could not create map roi"},
    },
)
def add_map_roi(roi: schema.MapROICreate) -> models.XRFFluorescenceMappingROI:
    """Add a new map roi"""
    try:
        return crud.create_map_roi(roi)
    except IndexError:
        raise HTTPException(status_code=404, detail="Sample not found")
    except Exception:
        logger.exception(f"Could not create map roi with payload `{roi}`")
        raise HTTPException(status_code=400, detail="Could not create map roi")


MAP_ROI_UPDATE_EXCLUDED = {"blSampleId": True}


@router.patch(
    "/rois/{xrfFluorescenceMappingROIId}",
    response_model=schema.MapROI,
    responses={
        404: {"description": "No such map roi"},
        400: {"description": "Could not update map roi"},
    },
)
def update_map_roi(
    xrfFluorescenceMappingROIId: int,
    roi: make_optional(
        schema.MapROICreate,
        exclude=MAP_ROI_UPDATE_EXCLUDED,
    ),
) -> schema.MapROI:
    """Update a Map"""
    try:
        return crud.update_map_roi(xrfFluorescenceMappingROIId, roi)
    except IndexError:
        raise HTTPException(status_code=404, detail="Map roi not found")
    except Exception:
        logger.exception(
            f"Could not update map roi `{xrfFluorescenceMappingROIId}` with payload `{roi}`"
        )
        raise HTTPException(status_code=400, detail="Could not update map roi")


@router.delete(
    "/rois/{xrfFluorescenceMappingROIId}",
    status_code=status.HTTP_204_NO_CONTENT,
    responses={
        404: {"description": "No such map ROI"},
        400: {"description": "Could not delete map ROI"},
    },
)
def delete_map_roi(
    xrfFluorescenceMappingROIId: int,
):
    """Delete a map ROI"""
    try:
        return crud.delete_map_roi(xrfFluorescenceMappingROIId)
    except IndexError:
        raise HTTPException(status_code=404, detail="Map ROI not found")
    except Exception as e:
        logger.exception(f"Could not delete map ROI `{xrfFluorescenceMappingROIId}`")
        raise HTTPException(
            status_code=400, detail=f"Could not delete map ROI: {str(e)}"
        )


@router.get("/composites", response_model=paginated(schema.CompositeMap))
def get_composite_maps(
    dataCollectionId: int = Depends(filters.dataCollectionId),
    dataCollectionGroupId: int = Depends(filters.dataCollectionGroupId),
    blSampleId: int = Depends(filters.blSampleId),
    blSubSampleId: int = Depends(filters.blSubSampleId),
    xfeFluorescenceCompositeId: int = Query(
        None, title="XfeFluorescenceCompositeId id"
    ),
    xrfFluorescenceMappingId: int = Query(None, title="XrfFluorescenceMapping id"),
    page: dict[str, int] = Depends(pagination),
) -> Paged[models.XRFFluorescenceMapping]:
    """Get a list of composite maps"""
    return crud.get_composite_maps(
        blSampleId=blSampleId,
        blSubSampleId=blSubSampleId,
        dataCollectionId=dataCollectionId,
        dataCollectionGroupId=dataCollectionGroupId,
        xfeFluorescenceCompositeId=xfeFluorescenceCompositeId,
        xrfFluorescenceMappingId=xrfFluorescenceMappingId,
        **page,
    )


@router.get("/composites/{xfeFluorescenceCompositeId}")
def get_composite_map(
    xfeFluorescenceCompositeId: int,
):
    """Get a composite map in image format"""
    composites = crud.get_composite_maps(
        xfeFluorescenceCompositeId=xfeFluorescenceCompositeId, skip=0, limit=1
    )
    try:
        composite = composites.first
    except IndexError:
        raise HTTPException(status_code=404, detail="Composite map not found")

    try:
        maps = {
            col: crud.get_maps(
                xrfFluorescenceMappingId=getattr(composite, col), skip=0, limit=1
            ).first
            for col in ["r", "g", "b"]
        }
    except IndexError:
        raise HTTPException(status_code=404, detail="Composite map child not found")

    image = crud.generate_composite_map_image(composite, maps)
    return StreamingResponse(image, media_type="image/png")


@router.post(
    "/composites",
    response_model=schema.CompositeMap,
    status_code=status.HTTP_201_CREATED,
)
def create_composite_map(composite: schema.CompositeMapCreate) -> schema.CompositeMap:
    """Create a new composite map"""
    try:
        return crud.create_composite_map(
            composite=composite,
        )
    except IndexError:
        raise HTTPException(status_code=404, detail="Composite map child not found")


COMPOSITE_MAP_UPDATE_EXCLUDED = {"r": True, "g": True, "b": True}


@router.patch(
    "/composites/{xfeFluorescenceCompositeId}",
    response_model=schema.CompositeMap,
    responses={
        404: {"description": "No such composite map"},
        400: {"description": "Could not update composite map"},
    },
)
def update_composite_map(
    xfeFluorescenceCompositeId: int,
    composite: make_optional(
        schema.CompositeMapCreate,
        exclude=COMPOSITE_MAP_UPDATE_EXCLUDED,
    ),
) -> schema.CompositeMap:
    """Update a Composite map"""
    try:
        return crud.update_composite_map(xfeFluorescenceCompositeId, composite)
    except IndexError:
        raise HTTPException(status_code=404, detail="Composite map not found")
    except Exception:
        logger.exception(
            f"Could not update composite map `{xfeFluorescenceCompositeId}` with payload `{composite}`"
        )
        raise HTTPException(status_code=400, detail="Could not update composite map")


@router.delete(
    "/composites/{xfeFluorescenceCompositeId}",
    status_code=status.HTTP_204_NO_CONTENT,
    responses={
        404: {"description": "No such composite map"},
        400: {"description": "Could not delete composite map"},
    },
)
def delete_composite_map(
    xfeFluorescenceCompositeId: int,
):
    """Delete a Composite map"""
    try:
        return crud.delete_composite_map(xfeFluorescenceCompositeId)
    except IndexError:
        raise HTTPException(status_code=404, detail="Composite map not found")
    except Exception:
        logger.exception(
            f"Could not delete composite map `{xfeFluorescenceCompositeId}`"
        )
        raise HTTPException(status_code=400, detail="Could not delete composite map")


@router.post(
    "/scalar",
    response_model=schema.Map,
    status_code=status.HTTP_201_CREATED,
    responses={
        404: {"description": "No such data collection"},
        400: {"description": "Could not create scalar map"},
    },
)
def add_scalar_map(scalarMap: schema.ScalarMapCreate) -> models.XRFFluorescenceMapping:
    """Create a new scalar map"""
    try:
        return crud.create_scalar_map(scalarMap)
    except IndexError:
        raise HTTPException(status_code=404, detail="Data collection not found")
    except Exception as e:
        logger.exception(
            f"Could not create scalar map `{scalarMap.scalar}` for {scalarMap.dataCollectionId}"
        )
        raise HTTPException(
            status_code=400, detail=f"Could not create scalar map: {str(e)}"
        )


@router.post(
    "/regenerate",
    status_code=status.HTTP_200_OK,
    response_model=schema.RegeneratedMapsResponse,
    responses={
        400: {"description": "Could not re-generate maps"},
    },
)
def regenerate_maps(
    regenerate: schema.RegenerateMaps,
) -> schema.RegeneratedMapsResponse:
    """Regenerate maps for a sample"""
    try:
        map_ids = crud.regenerate_maps(
            blSampleId=regenerate.blSampleId,
            dataCollectionId=regenerate.dataCollectionId,
            ev_per_bin=regenerate.ev_per_bin,
            ev_offset=regenerate.ev_offset,
        )
        return {"xrfFluorescenceMappingIds": map_ids}
    except Exception as e:
        logger.exception(
            f"Could not re-generate maps for sample `{regenerate.blSampleId}`"
        )
        raise HTTPException(
            status_code=400, detail=f"Could not re-generate maps: {str(e)}"
        )


@router.delete(
    "/{xrfFluorescenceMappingId}",
    status_code=status.HTTP_204_NO_CONTENT,
    responses={
        404: {"description": "No such map"},
        400: {"description": "Could not delete map"},
    },
)
def delete_map(
    xrfFluorescenceMappingId: int,
):
    """Delete a map"""
    try:
        return crud.delete_map(xrfFluorescenceMappingId)
    except IndexError:
        raise HTTPException(status_code=404, detail="Map not found")
    except Exception as e:
        logger.exception(f"Could not delete map `{xrfFluorescenceMappingId}`")
        raise HTTPException(status_code=400, detail=f"Could not delete map: {str(e)}")


@router.get("", response_model=paginated(schema.Map))
def get_maps(
    dataCollectionId: int = Depends(filters.dataCollectionId),
    dataCollectionGroupId: int = Depends(filters.dataCollectionGroupId),
    xrfFluorescenceMappingROIId: int = Depends(filters.xrfFluorescenceMappingROIId),
    blSampleId: int = Depends(filters.blSampleId),
    blSubSampleId: int = Depends(filters.blSubSampleId),
    xrfFluorescenceMappingId: int = Query(None, title="XrfFluorescenceMapping id"),
    page: dict[str, int] = Depends(pagination),
) -> Paged[models.XRFFluorescenceMapping]:
    """Get a list of maps"""
    return crud.get_maps(
        blSampleId=blSampleId,
        blSubSampleId=blSubSampleId,
        dataCollectionId=dataCollectionId,
        dataCollectionGroupId=dataCollectionGroupId,
        xrfFluorescenceMappingROIId=xrfFluorescenceMappingROIId,
        xrfFluorescenceMappingId=xrfFluorescenceMappingId,
        **page,
    )


MAP_UPDATE_EXCLUDED = {}


@router.patch(
    "/{xrfFluorescenceMappingId}",
    response_model=schema.Map,
    responses={
        404: {"description": "No such map"},
        400: {"description": "Could not update map"},
    },
)
def update_map(
    xrfFluorescenceMappingId: int,
    map_: make_optional(
        schema.MapBase,
        exclude=MAP_UPDATE_EXCLUDED,
    ),
) -> schema.Map:
    """Update a Map"""
    try:
        return crud.update_map(xrfFluorescenceMappingId, map_)
    except IndexError:
        raise HTTPException(status_code=404, detail="Map not found")
    except Exception:
        logger.exception(
            f"Could not update map `{xrfFluorescenceMappingId}` with payload `{map_}`"
        )
        raise HTTPException(status_code=400, detail="Could not update map")


@router.get("/histogram/{xrfFluorescenceMappingId}", response_model=schema.MapHistogram)
def get_map_histogram(
    xrfFluorescenceMappingId: int,
):
    """Get a map histogram"""
    maps = crud.get_maps(
        xrfFluorescenceMappingId=xrfFluorescenceMappingId, skip=0, limit=1
    )
    try:
        map_ = maps.first
    except IndexError:
        raise HTTPException(status_code=404, detail="Map not found")

    return crud.generate_histogram(map_)


@router.get("/pixel/{xrfFluorescenceMappingId}", response_model=schema.MapPixelValue)
def get_map_pixel_value(
    xrfFluorescenceMappingId: int,
    x: int = Query(None, title="X position"),
    y: int = Query(None, title="Y position"),
):
    """Get a map pixel value"""
    maps = crud.get_maps(
        xrfFluorescenceMappingId=xrfFluorescenceMappingId, skip=0, limit=1
    )
    try:
        map_ = maps.first
    except IndexError:
        raise HTTPException(status_code=404, detail="Map not found")

    data = crud.shape_map(map_)

    if y < len(data):
        if x < len(data[y]):
            return {
                "xrfFluorescenceMappingId": map_.xrfFluorescenceMappingId,
                "x": x,
                "y": y,
                "value": data[y][x],
            }


@router.get(
    "/pixel/values/{dataCollectionGroupId}", response_model=schema.MapPixelValues
)
def get_map_pixel_values(
    dataCollectionGroupId: int,
    xrfFluorescenceMappingROIId: int = Query(title="Y position"),
    x: Optional[int] = Query(None, title="X position"),
    y: Optional[int] = Query(None, title="Y position"),
):
    """Get a list of values from a series of maps for a single pixel"""
    maps = crud.get_maps(
        xrfFluorescenceMappingROIId=xrfFluorescenceMappingROIId,
        dataCollectionGroupId=dataCollectionGroupId,
        skip=0,
        limit=9999,
    )

    values = []
    for map_ in maps.results:
        data = crud.shape_map(map_)

        if x is not None and y is not None:
            if y < len(data):
                if x < len(data[y]):
                    values.append(float(data[y][x]))
                else:
                    values.append(0)
            else:
                values.append(0)
        else:
            values.append(float(numpy.sum(data)))

    return {
        "xrfFluorescenceMappingROIId": xrfFluorescenceMappingROIId,
        "dataCollectionGroupId": dataCollectionGroupId,
        "x": x,
        "y": y,
        "value": values,
    }


@router.get("/{xrfFluorescenceMappingId}")
def get_map(
    xrfFluorescenceMappingId: int,
):
    """Get a map in image format"""
    maps = crud.get_maps(
        xrfFluorescenceMappingId=xrfFluorescenceMappingId, skip=0, limit=1
    )
    try:
        map_ = maps.first
    except IndexError:
        raise HTTPException(status_code=404, detail="Map not found")

    image = crud.generate_map_image(map_)
    return StreamingResponse(image, media_type="image/png")
