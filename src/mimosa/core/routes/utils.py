import io
import os

from fastapi import Response
from fastapi.responses import FileResponse
from PIL import Image

from .responses import cache_response


def to_response(image: Image, format: str = None, mime_type: str = None):
    image_bytes = io.BytesIO()
    image.save(image_bytes, format=format if format else image.format)
    image_response = Response(
        content=image_bytes.getvalue(),
        media_type=(mime_type if mime_type else image.get_format_mimetype()),
    )
    cache_response(image_response)
    return image_response


def resize_image(image_path: str, maxSize: int):
    image = Image.open(image_path)
    image.thumbnail((maxSize, maxSize), Image.LANCZOS)
    return to_response(image)


def crop_image(image_path: str, crop_rect: str):
    crop_rect = list(map(int, crop_rect.split(",")))
    image = Image.open(image_path)
    if len(crop_rect) == 4:
        # Convert to (top, left, width ,height)
        crop_rect = (
            crop_rect[0],
            crop_rect[1],
            crop_rect[0] + crop_rect[2],
            crop_rect[1] + crop_rect[3],
        )
        cropped_image = image.crop(crop_rect)
    return to_response(cropped_image, format="png", mime_type="image/png")


def cache_image(image_path: str):
    response = FileResponse(image_path)
    cache_response(response)
    return response


def image_size_header(response: Response, image_path: str):
    if os.path.exists(image_path):
        image = Image.open(image_path)
        response.headers["x-original-size"] = f"{image.width},{image.height}"
    else:
        response.headers["x-original-size"] = ""
    return response
