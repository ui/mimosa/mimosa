from fastapi import Response
from pydantic import BaseModel


class HTTPError(BaseModel):
    detail: str

    class Config:
        schema_extra = {
            "example": {"detail": "HTTPException raised."},
        }


class Message(BaseModel):
    message: str


def cache_response(response: Response, max_age=10000):
    """Enable caching on this response"""
    response.headers["cache-control"] = f"max-age={max_age}"
    response.headers["etag"] = ""
    return response
