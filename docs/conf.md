Configuration is defined through environment variables. The base is configured vi a `.env` file and can be overriden by
either setting the environment variable directly or defining them in a `.env.local`

To start the application three additional variables are required:

```bash
AUTH_CONFIG=auth.yml
SECRET_KEY=dev_secret
SQLALCHEMY_DATABASE_URI=mysql+mysqlconnector://test:test@127.0.0.1/test

# Optionally enable CORS
CORS=true
```
