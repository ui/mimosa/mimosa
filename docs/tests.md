## Tests

---

### Coverage information

Test coverage information is available at
[https://ui.gitlab-pages.esrf.fr/mimosa/mimosa/coverage/](https://ui.gitlab-pages.esrf.fr/mimosa/mimosa/coverage/)

### Run test

In order to run the test, you need to have the test database up and running:

```bash
sudo docker run -p 3306:3306 -d --rm --name mariadb esrfbcu/mimosa-database:main
```

Install dev dependencies:

```bash
pip install -r requirements-dev.txt
```

Then, to run the tests, simply type:

```bash
pytest
```

To run the linting, type:

```bash
flake8
```
