# mimosa routes

---

## Routes documentation

To get a documentation gathering all the available routes, simply open the `/docs` route.

You may also check the automatic generated API documentation (Redoc) at:
[https://ui.gitlab-pages.esrf.fr/mimosa/mimosa/api/](https://ui.gitlab-pages.esrf.fr/mimosa/mimosa/api/)
