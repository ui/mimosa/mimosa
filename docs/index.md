# mimosa

[![License: LGPL v3](https://img.shields.io/badge/License-LGPL%20v3-blue.svg)](https://www.gnu.org/licenses/lgpl-3.0)

A FastAPI backend forked from [ISPyB](https://github.com/ispyb/py-ispyb)

---

## Main dependencies

- [**Python**](https://www.python.org/) 3.10+
- [**FastAPI**](https://fastapi.tiangolo.com/) - Web framework
- [**SQLAlchemy**](http://www.sqlalchemy.org/) - Database ORM
- [**pydantic**](https://pydantic-docs.helpmanual.io/) - Data validation and settings management
- [**MariaDB**](https://mariadb.org/) with a mimosa database schema
- [**mimosa Database Schema**](https://gitlab.esrf.fr/ui/mimosa/mimosa-database)
- [**mimosa Models**](https://gitlab.esrf.fr/ui/mimosa/mimosa-models)

---

Go to the [run the app](run.md) section to see how these dependencies can be satisfied, start exploring and contributing to the project.
