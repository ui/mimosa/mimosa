FROM node:20 as builder

WORKDIR /app
RUN git clone https://gitlab.esrf.fr/ui/mimosa/mimosa-ui . && \
    npm install -g pnpm && \
    pnpm install && \
    pnpm build
    
FROM  python:3.10-slim-bookworm

COPY . /mimosa
WORKDIR /mimosa

RUN apt-get update && apt-get install -y \
    libldap2-dev \
    libsasl2-dev \
    libmariadb-dev \
    build-essential \
    nginx && \
    pip install --no-cache-dir -e . && \
    apt-get remove -y build-essential gcc g++ && \
    apt-get clean autoclean && apt-get autoremove --yes && \
    rm -rf /var/lib/apt/lists/*

EXPOSE 8000
EXPOSE 80

COPY --from=builder /app/build /usr/share/nginx/html
COPY nginx.conf /etc/nginx/sites-available/default

ENV SQLALCHEMY_DATABASE_URI mysql+mysqlconnector://test:test@localhost/test
ENV SECRET_KEY secretkey
ENV AUTH_CONFIG tests/config/auth.yml

CMD nginx; gunicorn mimosa.app.main:app --workers 5 --worker-class uvicorn.workers.UvicornWorker --bind 0.0.0.0:8000
