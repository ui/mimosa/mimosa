# mimosa

A FastAPI backend for [mimosa-database](https://gitlab.esrf.fr/ui/mimosa/mimosa-database) and [daiquiri](https://gitlab.esrf.fr/ui/daiquiri). The frontend can be found in [mimosa-ui](https://gitlab.esrf.fr/ui/mimosa/mimosa-ui)

## Getting started

For dependencies, see [https://ui.gitlab-pages.esrf.fr/mimosa/mimosa/](https://ui.gitlab-pages.esrf.fr/mimosa/mimosa/).

For installation instructions, see [https://ui.gitlab-pages.esrf.fr/mimosa/mimosa/run](https://ui.gitlab-pages.esrf.fr/mimosa/mimosa/run)

The REST API is documented via [OpenAPI](https://ui.gitlab-pages.esrf.fr/mimosa/mimosa/api/)
